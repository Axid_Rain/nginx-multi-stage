FROM debian:10 as build-env

RUN apt update && apt install -y wget gcc make
RUN wget 'https://openresty.org/download/nginx-1.19.3.tar.gz' &&  tar -xzvf nginx-1.19.3.tar.gz && cd nginx-1.19.3/ && ./configure --without-http_gzip_module --without-http_rewrite_module  && make && make install

FROM debian:10

WORKDIR /usr/local/nginx/sbin/nginx
COPY --from=build-env /usr/local/nginx/sbin/nginx .
COPY ./mime.types ../../conf/mime.types
RUN mkdir ../../logs && touch ../../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]